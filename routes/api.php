<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(Array('user' => 'API\UserController'));
Route::apiResources(Array('author' => 'API\AuthorController'));
Route::apiResources(Array('libary' => 'API\LibaryController'));
Route::apiResources(Array('editor' => 'API\EditorController'));
Route::apiResources(Array('literaryGenre' => 'API\LiteraryGenreController'));
Route::apiResources(Array('book' => 'API\BookController'));

Route::get('profile', 'API\UserController@profile');
Route::put('profile', 'API\UserController@updateProfile');
Route::get('findUser', 'API\UserController@search');