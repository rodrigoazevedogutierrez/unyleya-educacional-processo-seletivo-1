<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Author extends Authenticatable
{
    //
    use Notifiable, HasApiTokens;

    protected $table = 'author';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'genre', 'nationality', 'birth_year', 'created_at', 'updated_at'
    ];
}
