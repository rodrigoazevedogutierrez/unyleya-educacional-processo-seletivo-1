<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Editor;

class EditorController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Editor::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $validate = Array(
            'name'  => 'required|string|max:191',
        );

        $this->validate($request, $validate);

        $editor = Array(  
                    'name'  => $request['name']
                );
            

        $editorObj = new Editor;
        $editorObj->name = $editor['name'];
        $editorObj->save();
    
        return Array('message' => 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $editor = Editor::findOrFail($id);
        
        $validate = Array(
            'name'  => 'required|string|max:191'
        );

        $this->validate($request, $validate);
         
        /**
         * incluir um picker para deixar de formatar a data
         */
        $editor->update($request->all());

        return Array('message' => 'Updated the editor info.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $editor = Editor::findOrFail($id);
        $editor->delete();

        return Array('message' => 'Editor Deleted');
    }
}
