<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->authorize('isAdmin');
        if (\Gate::allows('isAdmin') || \Gate::allows('isAuthor')) {
            return User::latest()->paginate(5);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $validate = Array(
                        'name'      => 'required|string|max:191',
                        'email'     => 'required|string|email|max:191|unique:users',
                        'password'  => 'required|string|min:6',
                    );

        $this->validate($request, $validate);
        

        $user = Array(  
                        'name'      => $request['name'], 
                        'email'     => $request['email'], 
                        'password'  => Hash::make($request['password']), 
                        'type'      => $request['type'],
                        'bio'       => $request['bio'],
                        'photo'     => $request['photo']
                    );


        
        $userObj = new User;
        $userObj->name = $user['name'];
        $userObj->email = $user['email'];
        $userObj->password = $user['password'];
        $userObj->type = $user['type'];
        $userObj->bio = $user['bio'];
        $userObj->photo = $user['photo'];
        $userObj->save();
        /* Metodo create do user esta retornando erro ,não econtra o campo password. */
        return Array('message' => 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = auth('api')->user();

        return $user;
    }
    
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $user = auth('api')->user();

        $currentPhoto = $user->photo;

        if($request->password == ''){
            unset($request['password']); 
        }
        
        $validate = Array(
            'name'      => 'required|string|max:191',
            'email'     => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password'  => 'sometimes|required|min:6',
        );

        $this->validate($request, $validate);

        if($request->photo != $currentPhoto){
            $name = time().'.'.explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            Image::make($request->photo)->save(public_path('img/profile/').$name);

            $request->merge(Array('photo' => $name));

            $userPhoto = public_path('img/profile/').$currentPhoto;
            
            if(file_exists($userPhoto)) {
                @unlink($userPhoto);
            }
        }

        if(!empty( $request->password ) ){
            $request->merge(Array('password' => Hash::make($request['password'])));

            $user->password = $request->password;
        
            $user->save();
        }
        
        $user->update($request->all());

        return Array('message' => 'Success');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        
        $validate = Array(
            'name'      => 'required|string|max:191',
            'email'     => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password'  => 'sometimes|min:6',
        );

        $this->validate($request, $validate);

        if(!empty( $request->password ) ){
            $request->merge(Array('password' => Hash::make($request['password'])));

            $user->password = $request->password;
        
            $user->save();
        }
        
        $user->update($request->all());

        return Array('message' => 'Updated the user info.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');

        $user = User::findOrFail($id);
        $user->delete();

        return Array('message' => 'User Deleted');
    }

    /**
     * Display the specified locate user at name.
     *
     * @param  string  $q
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        if($search = \Request::get('q')){
            $users = User::where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                        ->orWhere('email', 'LIKE', "%$search%")
                        ->orWhere('type', 'LIKE', "%$search%");
            })->paginate(20);
        } else {
            $users = User::latest()->paginate(5);
        }

        return $users;
    }
}
