<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Book;

class BookController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Book::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $validate = Array(
            'title'             => 'required|string|max:191', 
            'year'              => 'required|string|max:191', 
            'literary_genre_id' => 'required', 
            'editor_id'         => 'required', 
            'author_id'         => 'required'
        );

        $this->validate($request, $validate);

        $book = Array(  
                    'title'             => $request['title'], 
                    'year'              => $request['year'], 
                    'literary_genre_id' => $request['literary_genre_id'], 
                    'editor_id'         => $request['editor_id'], 
                    'author_id'         => $request['author_id']
                );
            
         /**
         * incluir um picker para deixar de formatar a data
         */
        $dateBook = explode('/',$book['year']);

        $bookObj = new Book;
        $bookObj->title             = $book['title'];
        $bookObj->year              = $dateBook[2] . '-' . $dateBook[1] . '-' . $dateBook[0];
        $bookObj->literary_genre_id = $book['literary_genre_id'];
        $bookObj->editor_id         = $book['editor_id'];
        $bookObj->author_id         = $book['author_id'];
        $bookObj->save();
    
        return Array('message' => 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        $book = Book::findOrFail($id);
        
        $validate = Array(
            'title'             => 'required|string|max:191', 
            'year'              => 'required|string|max:191', 
            'literary_genre_id' => 'required', 
            'editor_id'         => 'required', 
            'author_id'         => 'required'
        );

        $this->validate($request, $validate);
         
        $dateYear = explode('/',$request['birth_year']);
        $request['year'] = $dateYear[2] . '-' . $dateYear[1] . '-' . $dateYear[0];
        $book->update($request->all());

        return Array('message' => 'Updated the Book info.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $book = Book::findOrFail($id);
        $book->delete();

        return Array('message' => 'Book Deleted');
    }
}
