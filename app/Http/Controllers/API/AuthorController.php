<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Author;

class AuthorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Author::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = Array(
            'name'          => 'required|string|max:191',
            'genre'         => 'required|string|max:191',
            'nationality'   => 'required|string|max:191',
        );

        $this->validate($request, $validate);


        $author = Array(  
                    'name'          => $request['name'], 
                    'genre'         => $request['genre'], 
                    'nationality'   => $request['nationality'],
                    'birth_year'   => $request['birth_year']
                );
            
            
        /**
         * incluir um picker para deixar de formatar a data
         */
        $dateAuthorBirth = explode('/',$author['birth_year']);

        $authorObj = new Author;
        $authorObj->name = $author['name'];
        $authorObj->genre = $author['genre'];
        $authorObj->nationality = $author['nationality'];
        $authorObj->birth_year = $dateAuthorBirth[2] . '-' . $dateAuthorBirth[1] . '-' . $dateAuthorBirth[0];
        $authorObj->save();
    
        return Array('message' => 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $author = Author::findOrFail($id);
        
        $validate = Array(
            'name'          => 'required|string|max:191',
            'genre'         => 'required|string|max:191',
            'nationality'   => 'required|string|max:191',
        );

        $this->validate($request, $validate);
         
        /**
         * incluir um picker para deixar de formatar a data
         */
        $dateAuthorBirth = explode('/',$request['birth_year']);
        $request['birth_year'] = $dateAuthorBirth[2] . '-' . $dateAuthorBirth[1] . '-' . $dateAuthorBirth[0];
        $author->update($request->all());

        return Array('message' => 'Updated the user info.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $author = Author::findOrFail($id);
        $author->delete();

        return Array('message' => 'Author Deleted');
    }
}
