<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LiteraryGenre;

class LiteraryGenreController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return LiteraryGenre::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = Array(
            'genre'  => 'required|string|max:191',
        );

        $this->validate($request, $validate);

        $literaryGenre = Array(  
                    'genre'  => $request['genre']
                );
            

        $literaryGenreObj = new LiteraryGenre;
        $literaryGenreObj->genre = $literaryGenre['genre'];
        $literaryGenreObj->save();
    
        return Array('message' => 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $literaryGenre = LiteraryGenre::findOrFail($id);
        
        $validate = Array(
            'genre'  => 'required|string|max:191'
        );

        $this->validate($request, $validate);
         
        $literaryGenre->update($request->all());

        return Array('message' => 'Updated the Literary Genre info.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $literaryGenre = LiteraryGenre::findOrFail($id);
        $literaryGenre->delete();

        return Array('message' => 'Literary Genre Deleted');
    }
}
