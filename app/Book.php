<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Book extends Authenticatable
{
    //
    use Notifiable, HasApiTokens;

    protected $table = 'book';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
          
    protected $fillable = [
        'title', 'year', 'literary_genre_id', 'editor_id', 'author_id', 'created_at', 'updated_at'
    ];
}
