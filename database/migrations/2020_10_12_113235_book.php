<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Book extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('book', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamp('year');
            $table->integer('literary_genre_id')->unsigned();
            $table->integer('editor_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->foreign('literary_genre_id')->references('id')->on('literary_genre');
            $table->foreign('editor_id')->references('id')->on('editor');
            $table->foreign('author_id')->references('id')->on('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('book');
    }
}
